package pl.edu.mimuw.wikiontology.md346906.qualifier;

import java.util.HashSet;

public class Qualifier
{
	public static boolean isHuman(String text, HashSet<String> categories)
	{
		if (text == null)
			return false;
		
		if (text.contains("Persondata"))
			return true;
		
		for (String e : categories)
		{
			if (e.contains("births"))
				return true;
		}
		
		return false;
	}
}