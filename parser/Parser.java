package pl.edu.mimuw.wikiontology.md346906.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import pl.edu.mimuw.wikiontology.md346906.graph.Graph;
import pl.edu.mimuw.wikiontology.md346906.graph.Node;
import pl.edu.mimuw.wikiontology.md346906.qualifier.Qualifier;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Parser extends DefaultHandler
{
	private String temp, current, title, localisation;
	private boolean inBody, inTitle;
	
	public Parser(String localisation)
	{
		this.localisation = localisation;
		inBody = false;
		inTitle = false;
	}
	
	public void parseText(String text)
	{
		boolean isHuman = false;
		HashSet<String> categories = new HashSet<String>();
		HashSet<String> links = new HashSet<String>();
		Pattern pattern = Pattern.compile("\\[\\[Category:([^\\]]*)\\]\\]");
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) 
			categories.add(matcher.group(1));
		
		isHuman = Qualifier.isHuman(text, categories);
		
		Pattern pattern2 = Pattern.compile("\\[\\[([^\\]^\\|]*)([^\\]]*)\\]\\].{0}");
		Matcher matcher2 = pattern2.matcher(text);

		while (matcher2.find()) 
		{
			if (matcher2.group(1).contains("Category") == false)
			{
				links.add(matcher2.group(1));
			}
		}
		
		if (isHuman)
			Graph.addNode(new Node(title, categories, links));
	}
	
	public void characters(char[] buffer, int start, int length)
	{
		temp = new String(buffer, start, length);
		if (inBody)
			current += temp;
		if (inTitle)
			title = temp;
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{ 
		temp = new String();
		if (qName.equalsIgnoreCase("text"))
		{
			current = new String();
			inBody = true;
			inTitle = false;
		}
		if (qName.equalsIgnoreCase("title"))
		{
			title = new String();
			inBody = false;
			inTitle = true;
		}
	}
	
	public void endElement(String uri, String localName, String qName) throws SAXException
	{
		if (qName.equalsIgnoreCase("text"))
		{
			parseText(current);
			inBody = false;
		}
		if (qName.equalsIgnoreCase("title"))
			inTitle = false;
	}
	
	public void ParseXML() throws IOException, SAXException,ParserConfigurationException 
	{
		System.out.println("Please wait while database is loading");
		SAXParserFactory spfac = SAXParserFactory.newInstance();
		SAXParser sp = spfac.newSAXParser();
		Parser handler = new Parser(localisation);
		sp.parse(localisation, handler);
		doLinks();
		System.out.println("Loaded " + Graph.getNodes().size() + " articles.");
	}
	
	private void doLinks()
	{
		ArrayList<Node> graph = Graph.getNodes();
		for (Node e : graph)
			e.makeCategoryNeighbours();
	}
}
