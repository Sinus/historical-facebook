package pl.edu.mimuw.wikiontology.md346906.graph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

public class Node
{
	private boolean physicist;
	private String name;
	private ArrayList<Node> neighbours = new ArrayList<Node>();
	private int color;
	private int distance = 0;
	private Node parent;
	private HashSet<String> categories;
	private HashSet<String> links;
	
	public static final int black = 1;
	public static final int grey = 0;
	public static final int white = -1;
	
	public Node(String name, HashSet<String> categories, HashSet<String> links)
	{
		this.name = name;
		this.categories = categories;
		this.links = links;
	}
	
	public void makeCategoryNeighbours() //create neighbours list from set of links and decided if this is a physicist.
	{
		for (String e : categories)
			if (e.contains("physicist"))
				this.physicist = true;
		
		Node link;
		
		for (String e : links)
		{
			link = Graph.getNode(e);
			if (link != null)
				this.neighbours.add(link);
				
		}
	}
	
	public boolean isPhysicist()
	{
		return physicist == true;
	}
	
	public String getName()
	{
		return this.name;
	}

	public Stack<Node> BFS(String end, boolean filter)
	{
		Stack<Node> history = new Stack<Node>();
		this.color = grey;
		this.distance = 0;
		this.parent = null;
		Node endNode = null;
		ArrayDeque<Node> Q = new ArrayDeque<Node>();
		Q.add(this);
		Node u;
		
		while (Q.isEmpty() != true)
		{
			u = Q.pop();
			for (Node v : u.neighbours)
			{
				if ((filter == false) || (v.physicist == true)) // filter implies physicist
				{
					if (v.color == white)
					{
						v.color = grey;
						v.distance = u.distance + 1;
						v.parent = u;
						Q.offer(v);
					}
					if (v.name.equalsIgnoreCase(end))
						endNode = v;
				}
			}
			u.color = black;
		}
		if (endNode != null)
		{
			Node parent = endNode;
			while (parent != null)
			{
				history.push(parent);
				parent = parent.parent;
			}
		}
		return history;
	}
	
	//BFS methods
	public void setColor(int i)
	{
		this.color = i;
	}
	
	public void setDistance(Integer i)
	{
		this.distance = i;
	}
	
	public void setParent(Node n)
	{
		this.parent = n;
	}
	
	@Override
	public String toString()
	{
		String out = new String();
		out += this.name + System.lineSeparator();
		for (Node e : this.neighbours)
			out += e.name + System.lineSeparator();
		return out;
	}
}