package pl.edu.mimuw.wikiontology.md346906.graph;

import java.util.ArrayList;
import java.util.Stack;

public class Graph
{
	private static ArrayList<Node> nodes = new ArrayList<Node>();
	
	public static void addNode(Node x)
	{
		nodes.add(x);
	}
	
	public static ArrayList<Node> getNodes()
	{
		return nodes;
	}
	
	public static Node getNode(String name)
	{
		for (Node e : nodes)
		{
			if (e.getName().equalsIgnoreCase(name))
				return e;
		}
		return null;
	}
	
	public static String BFS(String start, String end, String filter)
	{
		boolean physicistOnly = false;
		start = start.replace("_", " ");
		end = end.replace("_", " ");
		filter = filter.toLowerCase();
		
		if (filter.equals("all"))
			physicistOnly = false;
		if (filter.equals("physicist"))
			physicistOnly = true;
		
		String out = new String();
		Stack<Node> history = null;
		Node startNode = null;
		for (Node e : nodes)
		{
			e.setColor(Node.white);
			e.setDistance(Integer.MAX_VALUE);
			e.setParent(null);
			if (e.getName().equalsIgnoreCase(start))
				startNode = e;
		}
		if (startNode != null)
			history = startNode.BFS(end, physicistOnly);
		
		if (history != null)
		{
			
			int length = history.size();
			if (length != 0)
			{
				length--;
				while (history.isEmpty() == false)
					out += history.pop().getName() + System.lineSeparator();
				out += "Path Length: " + length + System.lineSeparator();
			}
			else
				out += "No connection" + System.lineSeparator();
		}
		else
			out += "No connection" + System.lineSeparator();
		return out;
	}
}