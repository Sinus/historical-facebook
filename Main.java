package pl.edu.mimuw.wikiontology.md346906;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import pl.edu.mimuw.wikiontology.md346906.graph.Graph;
import pl.edu.mimuw.wikiontology.md346906.parser.Parser;

import java.util.*;

public class Main
{
	public static void main(String[] args)
	{
		Parser parser = new Parser(args[0]);
		
		try
		{
			parser.ParseXML();
		}
		catch (IOException | SAXException | ParserConfigurationException e)
		{
			System.out.println("There was an error.");
			e.printStackTrace();
		}
		
		String filter, start, end;
		
		Scanner scanner = new Scanner(System.in);
		
		while (true)
		{
			filter = scanner.next();
			start = scanner.next();
			end = scanner.next();
			System.out.println(Graph.BFS(start, end, filter));
		}
	}
}